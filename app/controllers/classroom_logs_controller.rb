class ClassroomLogsController < ApplicationController
  # GET /classroom_logs
  # GET /classroom_logs.json
  def index
    @classroom_logs = ClassroomLog.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @classroom_logs }
    end
  end

  # GET /classroom_logs/1
  # GET /classroom_logs/1.json
  def show
    @classroom_log = ClassroomLog.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @classroom_log }
    end
  end

  # GET /classroom_logs/new
  # GET /classroom_logs/new.json
  def new
    @classroom_log = ClassroomLog.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @classroom_log }
    end
  end

  # GET /classroom_logs/1/edit
  def edit
    @classroom_log = ClassroomLog.find(params[:id])
  end

  # POST /classroom_logs
  # POST /classroom_logs.json
  def create
    @classroom_log = ClassroomLog.new(params[:classroom_log])

    respond_to do |format|
      if @classroom_log.save
        format.html { redirect_to @classroom_log, notice: 'Classroom log was successfully created.' }
        format.json { render json: @classroom_log, status: :created, location: @classroom_log }
      else
        format.html { render action: "new" }
        format.json { render json: @classroom_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /classroom_logs/1
  # PUT /classroom_logs/1.json
  def update
    @classroom_log = ClassroomLog.find(params[:id])

    respond_to do |format|
      if @classroom_log.update_attributes(params[:classroom_log])
        format.html { redirect_to @classroom_log, notice: 'Classroom log was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @classroom_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classroom_logs/1
  # DELETE /classroom_logs/1.json
  def destroy
    @classroom_log = ClassroomLog.find(params[:id])
    @classroom_log.destroy

    respond_to do |format|
      format.html { redirect_to classroom_logs_url }
      format.json { head :no_content }
    end
  end
end
