class CourseHistoryTablesController < ApplicationController
  # GET /course_history_tables
  # GET /course_history_tables.json
  def index
    @course_history_tables = CourseHistoryTable.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @course_history_tables }
    end
  end

  # GET /course_history_tables/1
  # GET /course_history_tables/1.json
  def show
    @course_history_table = CourseHistoryTable.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @course_history_table }
    end
  end

  # GET /course_history_tables/new
  # GET /course_history_tables/new.json
  def new
    @course_history_table = CourseHistoryTable.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @course_history_table }
    end
  end

  # GET /course_history_tables/1/edit
  def edit
    @course_history_table = CourseHistoryTable.find(params[:id])
  end

  # POST /course_history_tables
  # POST /course_history_tables.json
  def create
    @course_history_table = CourseHistoryTable.new(params[:course_history_table])

    respond_to do |format|
      if @course_history_table.save
        format.html { redirect_to @course_history_table, notice: 'Course history table was successfully created.' }
        format.json { render json: @course_history_table, status: :created, location: @course_history_table }
      else
        format.html { render action: "new" }
        format.json { render json: @course_history_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /course_history_tables/1
  # PUT /course_history_tables/1.json
  def update
    @course_history_table = CourseHistoryTable.find(params[:id])

    respond_to do |format|
      if @course_history_table.update_attributes(params[:course_history_table])
        format.html { redirect_to @course_history_table, notice: 'Course history table was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @course_history_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_history_tables/1
  # DELETE /course_history_tables/1.json
  def destroy
    @course_history_table = CourseHistoryTable.find(params[:id])
    @course_history_table.destroy

    respond_to do |format|
      format.html { redirect_to course_history_tables_url }
      format.json { head :no_content }
    end
  end
end
