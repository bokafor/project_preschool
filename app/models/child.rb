class Child < ActiveRecord::Base
  attr_accessible :age_group, :child_fname, :child_lname

  has_many :classroom_logs #for child name drop down in Classroom_log table
  has_many :course_history_tables #for age group drop down in Course_history table
end
