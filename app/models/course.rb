class Course < ActiveRecord::Base
  attr_accessible :course_description, :course_name

  has_many :course_history_tables #for course drop down in Course_history table
end
