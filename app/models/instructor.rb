class Instructor < ActiveRecord::Base
  attr_accessible :instructor_email, :instructor_fname, :instructor_lname, :instructor_phone

  has_many :classroom_logs #for instructor name drop down in Classroom_log table
  has_many :course_history_tables #for instructor name drop down in Course_history table
end
