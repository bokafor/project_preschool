class CreateClassroomLogs < ActiveRecord::Migration
  def change
    create_table :classroom_logs do |t|
      t.integer :child_id
      t.integer :instructor_id
      t.integer :classroom_number

      t.timestamps
    end
  end
end
