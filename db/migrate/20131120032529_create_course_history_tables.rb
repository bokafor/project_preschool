class CreateCourseHistoryTables < ActiveRecord::Migration
  def change
    create_table :course_history_tables do |t|
      t.integer :instructor_id
      t.integer :course_id
      t.string :course_status
      t.integer :child_id

      t.timestamps
    end
  end
end
