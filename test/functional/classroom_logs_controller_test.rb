require 'test_helper'

class ClassroomLogsControllerTest < ActionController::TestCase
  setup do
    @classroom_log = classroom_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:classroom_logs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create classroom_log" do
    assert_difference('ClassroomLog.count') do
      post :create, classroom_log: { child_id: @classroom_log.child_id, classroom_number: @classroom_log.classroom_number, instructor_id: @classroom_log.instructor_id }
    end

    assert_redirected_to classroom_log_path(assigns(:classroom_log))
  end

  test "should show classroom_log" do
    get :show, id: @classroom_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @classroom_log
    assert_response :success
  end

  test "should update classroom_log" do
    put :update, id: @classroom_log, classroom_log: { child_id: @classroom_log.child_id, classroom_number: @classroom_log.classroom_number, instructor_id: @classroom_log.instructor_id }
    assert_redirected_to classroom_log_path(assigns(:classroom_log))
  end

  test "should destroy classroom_log" do
    assert_difference('ClassroomLog.count', -1) do
      delete :destroy, id: @classroom_log
    end

    assert_redirected_to classroom_logs_path
  end
end
