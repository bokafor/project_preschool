require 'test_helper'

class CourseHistoryTablesControllerTest < ActionController::TestCase
  setup do
    @course_history_table = course_history_tables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:course_history_tables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create course_history_table" do
    assert_difference('CourseHistoryTable.count') do
      post :create, course_history_table: { child_id: @course_history_table.child_id, course_id: @course_history_table.course_id, course_status: @course_history_table.course_status, instructor_id: @course_history_table.instructor_id }
    end

    assert_redirected_to course_history_table_path(assigns(:course_history_table))
  end

  test "should show course_history_table" do
    get :show, id: @course_history_table
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @course_history_table
    assert_response :success
  end

  test "should update course_history_table" do
    put :update, id: @course_history_table, course_history_table: { child_id: @course_history_table.child_id, course_id: @course_history_table.course_id, course_status: @course_history_table.course_status, instructor_id: @course_history_table.instructor_id }
    assert_redirected_to course_history_table_path(assigns(:course_history_table))
  end

  test "should destroy course_history_table" do
    assert_difference('CourseHistoryTable.count', -1) do
      delete :destroy, id: @course_history_table
    end

    assert_redirected_to course_history_tables_path
  end
end
